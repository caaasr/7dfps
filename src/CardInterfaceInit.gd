extends Control

func _ready():
	SoundManager.play_ambient1()
	# Sending card container references to the card manager.
	CardManager.mag_1 = $CardContainer1
	CardManager.mag_2 = $CardContainer2
	CardManager.mag_3 = $CardContainer3
	CardManager.coll = $CardCollection
	# Initializing the containers.
	for card in CardManager.card_container_1:
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = card
		$CardContainer1/CardContainerV.add_child(cardCopy)
	for card in CardManager.card_container_2:
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = card
		$CardContainer2/CardContainerV.add_child(cardCopy)
	for card in CardManager.card_container_3:
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = card
		$CardContainer3/CardContainerV.add_child(cardCopy)
	for card in CardManager.card_collection:
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = card
		$CardCollection/CollectionV.add_child(cardCopy)
		
func _process(_delta):
	$TexRect1/MagLabel1.text = str(CardManager.mag_1.get_child(0).get_children().size()) + " | 10"
	$TexRect2/MagLabel2.text = str(CardManager.mag_2.get_child(0).get_children().size()) + " | 10"
	$TexRect3/MagLabel3.text = str(CardManager.mag_3.get_child(0).get_children().size()) + " | 10"
