extends KinematicBody

export(NodePath) var parent_nodepath
var path_parent = null
var path = []

enum state {IDLE, ATTACKING}
var debug_state = ["IDLE", "ATTACKING"] #for printing state in a readable manner

export(state) var imp_state
export(bool) var force_state = false

var speed = 10
var grav = -9.8

onready var player = get_node("../Player")
onready var navmesh = get_node("../navmesh")
onready var fireball = preload("res://assets/firbals.tscn")
onready var launchpos = $launchpos

var state_switch_cooldown = 5

export(int) var health = 80
var card = null

export(bool) var debug_lines = false

func _ready():
	path_parent = get_node(parent_nodepath)
	var path_transforms = path_parent.get_children()
	
	for i in range(path_transforms.size()):
		path.push_back(path_transforms[i].transform.origin)
		
	self.transform.origin = path[0] * Vector3(1, 0, 1) + Vector3(0, 1.5, 0)
		
#	path_to_node = navmesh.get_simple_path(self.transform.origin, player.transform.origin)
#	for n in range(path_to_node.size()):
#		print(path_to_node[n])
		
func move(delta, path_array, path_index):
	#figure out how far to move and in what direction
	#using move and slide because it makes things easier
	var dir = Vector3(0, 0, 0)
	if path_array != null and !path_array.empty() and path_index < path_array.size():
		#print("MOVING")
		dir = path_array[path_index] - self.transform.origin
		dir = dir.normalized() * speed * delta
	
		#figure out how much distance is left to the destination node
		var remainder = (path_array[path_index] - (self.transform.origin + dir))
		var dest_dir = self.transform.origin.direction_to(path_array[path_index])
		

		#if the direction to the destination node and direction to it after the movement are pointing in opposite directions
		#we know we've overshot
		var overshot = dest_dir.dot(remainder.normalized())

		if (overshot < 0):

			#we've gone too far along the path to the current node, and need to turn to the next node
			#so move the rest of the distance to to the current node before switching
			move_and_collide((path_array[path_index] - self.transform.origin) * Vector3(1, 0, 1))
			
			path_index += 1

			#if there are nodes in the path left
			if (path_index < path_array.size()):
				dir = self.transform.origin.direction_to(path_array[path_index])
			else:
				dir = self.transform.origin.direction_to((path_array[path_index - 1]))
				
			dir *= remainder.length()

	move_and_collide(dir * Vector3(1, 0, 1))
	
	#vertical alignment check, might not be needed
	if path_index < path_array.size():
		var vert = self.transform.origin.direction_to(path_array[path_index]).dot(self.transform.basis.y)
		if vert < -.999 or vert > .999:
			path_index += 1
#	#check to see if we're at the end of the path to stop violenty vibrating
#	if path_array != null and path_array.size() > 0 and (self.transform.origin - path_array[path_array.size() - 1]).length() < .1:
#		return path_index + 1
#
	return path_index
	
func gravity(delta):
	move_and_collide(Vector3(0, grav, 0) * delta)

#orients imp towards loc
func look_at_horizontal(loc: Vector3):
	look_at(loc * Vector3(1, 0, 1) + self.transform.origin * Vector3(0, 1, 0), Vector3(0, 1, 0))

var index = 0
func _idle(delta):
	index = move(delta, path, index) % path.size()
	
	look_at_horizontal(path[index])

	gravity(delta)
	if debug_lines:
		DrawLine3D.DrawLine(self.transform.origin, path[index], Color(1, 0, 0), delta)
	

func _fire():
	SoundManager.play_fireball()
	var fball = fireball.instance()
	fball.fireball_init(25, player.transform.origin)
	fball.transform.origin = launchpos.global_transform.origin
	self.get_parent().add_child(fball)
	
func hit(dmg):
	#print("I was hit for ", dmg)
	health -= dmg
	if health <= 0:
		card = load("res://assets/card_drop.tscn")
		_death()
		
func _death():

	var card_type = CardManager.roll_type()
	var card_id = CardManager.roll_card(card_type)
	var card_drop = card.instance()
	card_drop.set_card_type(card_type, card_id)
	self.get_parent().add_child(card_drop)
	#card_drop._ready()

	#this will need to be tuned for each enemy
	card_drop.transform.origin = Vector3(self.transform.origin.x, 0.5, self.transform.origin.z)

	SoundManager.play_enemy_death()
	queue_free()
	
	
var path_to_node = null
var ptn_index = 1
var attack_cooldown = 0

func _attack(delta):
	if path_to_node == null:
		path_to_node = navmesh.get_simple_path(self.transform.origin, player.transform.origin)
		
	if attack_cooldown < 2:
		ptn_index = move(delta, path_to_node, ptn_index)
		if debug_lines:
			DrawLine3D.DrawLine(self.transform.origin, path_to_node[path_to_node.size() - 1], Color(1, 0, 0), delta)
		gravity(delta)
	elif attack_cooldown > 2.1:
		attack_cooldown = 0
		_fire()
		path_to_node = navmesh.get_simple_path(self.transform.origin, player.transform.origin)
		ptn_index = 1
		
	look_at_horizontal(player.transform.origin)
	attack_cooldown += delta
	
	if ptn_index >= path_to_node.size():
		path_to_node = navmesh.get_simple_path(self.transform.origin, player.transform.origin)
		ptn_index = 1


func _update_state(delta):
	if state_switch_cooldown >= 5 and !force_state:
		#print("trying to updating state")
		var p_dist = (self.transform.origin - player.transform.origin).length()
		var p_dir = self.transform.origin.direction_to(player.transform.origin)
		
		if p_dist < 15 or (p_dir.dot(-self.transform.basis.z) > 0 and p_dist < 70):
			if imp_state != state.ATTACKING:
				state_switch_cooldown = 0
				#print("update successful")
			imp_state = state.ATTACKING
		else:
			if imp_state != state.IDLE:
				state_switch_cooldown = 0
				#print("update successful")
			imp_state = state.IDLE
	
	state_switch_cooldown += delta
		
	

func _physics_process(delta):
	_update_state(delta)
	if imp_state == state.IDLE:
		_idle(delta)
	elif imp_state == state.ATTACKING:
		_attack(delta)
		
	if debug_lines:
		DrawLine3D.DrawLine(self.transform.origin, self.transform.origin - self.transform.basis.z * 4, Color(0, 0, 1), delta)

