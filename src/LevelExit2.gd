extends MeshInstance

var entered = false

func _on_ExitArea_body_entered(body):
	if body.name == 'Player' and entered == false:
		# Incrementing to the next level and fading out.
		entered = true

		Pval.reset_to_base()

		WorldManager.curr_level += 1
		SoundManager.play_portal()
		$FadeAnim.play('fade')

func _on_FadeAnim_animation_finished(anim_name):
	# Sending the player to the card interface.
	SoundManager.stop_level()
	 # Hopefully this line doesn't cause problems.
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().change_scene('res://interface/EndScreen.tscn')
