extends KinematicBody

#camera controls
var rot_x = 0
var rot_y = 0
var degrees_per_turn = 0.022 #this is source engine sensitivity
var yaw = degrees_per_turn * (PI / 180)

onready var cam = $head
onready var ray = $head/guncast

onready var mag_icon_1 = $HUD/MagDisplay/MagIcon1
onready var mag_icon_2 = $HUD/MagDisplay/MagIcon2
onready var mag_icon_3 = $HUD/MagDisplay/MagIcon3
onready var swap_anims = $HUD/MagDisplay/SwapAnims
onready var hud_health = $HUD/HealthDisplay

onready var arq_anims = $head/ArqAnims
onready var arq_muzzle = $head/Arquebor/MuzzleFlash

onready var hit_particles = preload("res://assets/hit_effect.tscn")

#movement controls
#var vel = Vector3(0, 0, 0)
var yvel = 0

func _ready():
	ray.set_cast_to(Vector3(0, 0, -Pval.firerange))
	CardManager.apply_mag()
	swap_anims.play("swap1")
	arq_anims.play("new")

#shooting cooldowns
#fire range is set in Pval
#fire delay is set in Pval
var rem_firedelay = 0
var firetoggle = false

#var shoot_i = 0

func shoot():
	ray.set_cast_to(Vector3(0, 0, -Pval.firerange))
	SoundManager.play_shoot()
	arq_anims.play("fire")
	arq_muzzle.show()
	var col = ray.get_collider()
	var col_point = ray.get_collision_point()
	#shoot_i += 1
	#print("shooting", shoot_i)
	if ray.is_colliding() and col.has_method("hit"):
		col.hit(Pval.firedmg)
		
	if ray.is_colliding():
		var dust = hit_particles.instance()
		dust.transform.origin = col_point
		self.get_parent().add_child(dust)
		
		
		#add effect at vec3 of collision

var mag_swap_cooldown = 0 # Turned this to 0 for now to avoid anim conflict
var mag_swap_delay = 0
#swaps mag on mag num change
func swap_mag(mag_num):
	if Pval.cur_mag != Pval.get_mag_from_num(mag_num) and mag_swap_delay <= 0:
		Pval.cur_mag = Pval.get_mag_from_num(mag_num)
		Pval.reset_to_base()
		CardManager.apply_mag()
		mag_swap_delay = mag_swap_cooldown
		
func hit(dmg):
	SoundManager.play_hurt()
	Pval.dmg_player(dmg)
	
func _die():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().change_scene('res://interface/GameOver.tscn')

func _input(event):
	#CAMERA CODE
	if event is InputEventMouseMotion:
		rot_x += -event.relative.x * (yaw * Pval.sens)
		rot_y += -event.relative.y * (yaw * Pval.sens)
		rot_y = clamp(rot_y, -0.5 * PI, 0.5 * PI)
		
		transform.basis = Basis()
		cam.transform.basis = Basis()
		rotate_object_local(Vector3(0, 1, 0), rot_x)
		cam.rotate_object_local(Vector3(1, 0, 0), rot_y)
		

func _physics_process(delta):
	#MOVE CODE
	#reset movement velocity
	var vel = Vector3(0, 0, 0)

	if Input.is_action_pressed("ui_up"):
		vel += Vector3(0, 0, -1)
		if !Sound.get_child(9).is_playing() and Pval.can_jump:
			SoundManager.play_footsteps()
	if Input.is_action_pressed("ui_down"):
		vel += Vector3(0, 0, 1)
		if !Sound.get_child(9).is_playing() and Pval.can_jump:
			SoundManager.play_footsteps()
	if Input.is_action_pressed("ui_left"):
		vel += Vector3(-1, 0, 0)
		if !Sound.get_child(9).is_playing() and Pval.can_jump:
			SoundManager.play_footsteps()
	if Input.is_action_pressed("ui_right"):
		vel += Vector3(1, 0, 0)
		if !Sound.get_child(9).is_playing() and Pval.can_jump:
			SoundManager.play_footsteps()
		

	var dir = vel.x * self.global_transform.basis.x + vel.z * self.global_transform.basis.z
	move_and_collide(dir.normalized() * delta * Pval.speed)
	
	#JUMP CODE
	if Input.is_action_just_pressed("ui_accept") and Pval.can_jump == 1:
		SoundManager.play_jump()
		yvel = Pval.jump_speed
		Pval.can_jump = 0
		
	#GRAVITY
	#this need to be improved to better check for collisions below the player
	var col = move_and_collide(Vector3(0, 1, 0) * yvel * delta)
	yvel += Pval.gravity * delta
	if col and (self.transform.origin.y - col.position.y) > 0:
		yvel = 0
		Pval.can_jump = 1
		
	#SHOOTING 
	if Input.is_action_pressed("ui_leftclick"):
		if rem_firedelay <= 0:
			shoot()
			rem_firedelay = Pval.firedelay
			
	if rem_firedelay > 0:
		rem_firedelay -= delta
		
	#MAG SWAPPING
	if Input.is_action_just_pressed("numrow_1"):
		swap_mag(1)
		SoundManager.play_load()
		swap_anims.play("swap1")
	if Input.is_action_just_pressed("numrow_2"):
		swap_mag(2)
		SoundManager.play_load()
		swap_anims.play("swap2")
	if Input.is_action_just_pressed("numrow_3"):
		swap_mag(3)
		SoundManager.play_load()
		swap_anims.play("swap3")
		
	if mag_swap_delay > 0:
		mag_swap_delay -= delta
		
func _process(_delta):
	if Pval.cur_health <= 0:
		_die()
	hud_health.text = str(floor(Pval.cur_health))

func _on_ArqAnims_animation_finished(anim_name):
	arq_muzzle.hide()
	arq_anims.play("new")
