extends Button

func _on_BeginRun_mouse_entered():
	SoundManager.play_hover()

func _on_BeginRun_pressed():
	# print(Pval.cur_health)
	SoundManager.play_click()
	SoundManager.stop_ambient1()
	SoundManager.play_level()
	# Pval.reset_to_base()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().change_scene('res://levels/Level1.tscn')
