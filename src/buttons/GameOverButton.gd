extends Button

func _on_GameOverButton_pressed():
	get_tree().quit() # Workaround line for instant game over bug.
	SoundManager.play_click()
	Pval.reset_to_base()
	CardManager.card_container_1 = []
	CardManager.card_container_2 = []
	CardManager.card_container_3 = []
	CardManager.card_collection = []
	SoundManager.stop_level()
	WorldManager.curr_level = 1
	Pval.cur_health = 50
	# print(Pval.cur_health)
	get_tree().change_scene('res://interface/MainMenu.tscn')

func _on_GameOverButton_mouse_entered():
	SoundManager.play_hover()
