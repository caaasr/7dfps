extends Button

func _on_ExitGame_mouse_entered():
	SoundManager.play_hover()

func _on_ExitGame_pressed():
	SoundManager.play_click()
	get_tree().quit()
