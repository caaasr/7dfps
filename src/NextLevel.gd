extends TextureButton

func _on_NextLevel_pressed():
	SoundManager.play_click()
	# Updating card containers in the singleton.
	var new_mag_1 = []
	var new_mag_2 = []
	var new_mag_3 = []
	var new_coll = []
	for card in CardManager.mag_1.get_child(0).get_children():
		new_mag_1.append(card.card_ID)
	for card in CardManager.mag_2.get_child(0).get_children():
		new_mag_2.append(card.card_ID)
	for card in CardManager.mag_3.get_child(0).get_children():
		new_mag_3.append(card.card_ID)
	for card in CardManager.coll.get_child(0).get_children():
		new_coll.append(card.card_ID)
	CardManager.card_container_1 = new_mag_1
	CardManager.card_container_2 = new_mag_2
	CardManager.card_container_3 = new_mag_3
	CardManager.card_collection = new_coll
	# Moving to the next level.
	SoundManager.stop_ambient1()
	SoundManager.play_level()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().change_scene(WorldManager.levelDict[WorldManager.curr_level])
	
	Pval.cur_mag = Pval.get_mag_from_num(0)
	CardManager.apply_mag()

func _on_NextLevel_mouse_entered():
	SoundManager.play_hover()
