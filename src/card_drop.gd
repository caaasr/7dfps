extends Area

var r_speed = PI
var lifetime = 0
var lifetime_offset = 0
onready var card = $card
onready var glow = $glow
onready var player = get_node("/root/Level/Player")

#card colors
var common_mat = preload("res://assets/cards/common.tres")
var rare_mat = preload("res://assets/cards/rare.tres")
var unique_mat = preload("res://assets/cards/unique.tres")

#card info
export var card_id = 1
#this should get set in the ready function from the card dictionary
export var rarity = 0

#idle animation
func _idle(delta):
	card.rotate(Vector3(0, 1, 0), r_speed * delta)
	card.transform.origin.y = self.transform.origin.y + ((sin((lifetime + lifetime_offset) * 2) + 1) / 2) * .5
	
#when the card gets picked up
func pickup():
	SoundManager.play_unload()
	CardManager.card_collection.push_back(card_id)
	queue_free()
	
func set_card_type(type, id):
	rarity = type
	card_id = id
	
var offdist = 25
var ondist = 20

func lighting():
	
	var player_dist = (self.transform.origin - player.transform.origin).length()
	if player_dist > offdist:
		glow.hide()
	elif player_dist <= offdist and player_dist >= ondist:
		glow.show()
		glow.light_energy = 1.0 - ((player_dist - ondist) / (offdist - ondist))
	else:
		glow.show()
		glow.light_energy = 1.0
	
	
func _ready():	
	lifetime_offset = rand_range(-1.0, 1.0)
	card.rotate(Vector3(0, 1, 0), lifetime_offset * (2 * PI))
	card.set_material_override(common_mat)
	
	#figure out how to set glow color from material
	if rarity == 0:
		glow.light_color = Color(0, 0, 1)
		card.set_material_override(common_mat)

	elif rarity == 1:
		glow.light_color = Color(1, 0, 0)
		card.set_material_override(rare_mat)

	else:
		glow.light_color = Color(1, 1, 0)
		card.set_material_override(unique_mat)


func _process(delta):
	_idle(delta)
	lifetime += delta
	lighting()
	

func _on_card_drop_body_entered(body):
	if body.name == "Player":
		#print("picked up: ", CardManager.descDict[card_id])
		pickup()
