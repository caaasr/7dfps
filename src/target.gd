extends KinematicBody

var card = null

#this is an example for enemy types
var health = 10

func hit(dmg):
	#print("I was hit for ", dmg)
	health -= dmg
	if health <= 0:
		card = load("res://assets/card_drop.tscn")
		_death()
		
func _death():

	var card_type = CardManager.roll_type()
	var card_id = CardManager.roll_card(card_type)
	var card_drop = card.instance()
	card_drop.set_card_type(card_type, card_id)
	self.get_parent().add_child(card_drop)
	#card_drop._ready()

	#this will need to be tuned for each enemy
	card_drop.transform.origin = Vector3(self.transform.origin.x, 0.5, self.transform.origin.z)

	queue_free()
