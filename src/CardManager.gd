extends Node

# Scene paths.
const cardBaseScene = preload('res://interface/Card.tscn')
const cardInterfaceScene = preload('res://interface/CardInterface.tscn')

# Will be initialized as references to card container nodes.
var mag_1
var mag_2
var mag_3
var coll

# Card containers.
var card_container_1 = []
var card_container_2 = []
var card_container_3 = []
var card_collection = []

#drop rates, should add up to 1
#array is [common, rare, unique]
var drop_rate = [0.65, 0.3, 0.05]

#card rarities, [common, rare, unique]
var rarity_table = [[2, 3, 6], [1, 5], [4]]

func roll_type():
	var roll = rand_range(0, 1.0)
	var acc = 0
	var type = 0
	while acc < roll:
		acc += CardManager.drop_rate[type]
		type += 1
		
	type -= 1
		
	#print("roll: ", roll, " type: ", type)
	
	return type
	
func roll_card(type):
	var card_id = rarity_table[type][randi() % rarity_table[type].size()]
	return card_id


func apply_mag():
	Pval.reset_to_base()
	for i in range(Pval.cur_mag.size()):
		funcDict[Pval.cur_mag[i]].call_func()

# Dictionary of card descriptions.
var descDict = {
	0 : '',
	1 : 'Increase speed by 5.',
	2 : 'Increase damage by 5.',
	3 : 'Increase health by 5.',
	4 : 'Increase firerate by 2 shots per second.',
	5 : 'Increase jump speed by 5.',
	6 : 'Increase range by 10.'
}

# Dictionary of card functions.
var funcDict = {
	0 : funcref(self, "null_func"),
	1 : funcref(self, "speed_inc"),
	2 : funcref(self, "dmg_inc"),
	3 : funcref(self, "hp_inc"),
	4 : funcref(self, "sps_inc"),
	5 : funcref(self, "jmp_inc"),
	6 : funcref(self, "range_inc")
}

# Card functions.
func null_func():
	print("you shouldn't have called this, this card is not supposed be in any deck")

func speed_inc():
	Pval.speed += 5
	#print('speed increased by 5 to ', Pval.speed)
	
func dmg_inc():
	Pval.firedmg += 5
	#print('damage increased by 5 to ', Pval.firedmg)

func hp_inc():
	Pval.set_health(Pval.max_health + 5)
	#print('health increased by 5 to ', Pval.max_health)
	
func sps_inc():
	Pval.set_firerate(Pval.firerate + 2)
	#print('firerate increased by 4 to ', Pval.firerate)
	
func jmp_inc():
	Pval.jump_speed += 5
	#print('jump speed increased by 5 to ', Pval.jump_speed)
	
func range_inc():
	Pval.firerange += 5
	#print('jump speed increased by 5 to ', Pval.jump_speed)
